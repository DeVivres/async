﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Project.DAL.Entities
{
    public class Project
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [MinLength(15)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        [Range(1, int.MaxValue)]
        public int AuthorId { get; set; }
        [Range(1, int.MaxValue)]
        public int TeamId { get; set; }
        [Required]
        public int IncomeFromProject { get; set; }
    }
}
