﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Project.DAL.Entities
{
    public class TaskState
    {
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string Value { get; set; }
    }
}
