﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Project.DAL.Repositories
{
    public class UsersRepository : IRepository<User>
    {
        private readonly DatabaseContext _context;

        public UsersRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task Create(User item)
        {
            await _context.Users.AddAsync(item);
        }

        public async Task<bool> Delete(int id)
        {
            var item = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if (item == null)
            {
                return false;
            }

            _context.Users.Remove(item);
            return true;
        }

        public async Task<User> Get(int id)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<IEnumerable<User>> GetAll() 
        { 
            return await _context.Users.ToListAsync(); 
        }

        public async Task<bool> Update(User item)
        {
            var exists = await _context.Users.ContainsAsync(item);

            if (!exists)
            {
                return false;
            }

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
