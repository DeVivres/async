﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Project.DAL.Repositories
{
    public class TaskStatesRepository : IRepository<TaskState>
    {
        private readonly DatabaseContext _context;

        public TaskStatesRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task Create(TaskState item)
        {
            await _context.States.AddAsync(item);
        }

        public async Task<bool> Delete(int id)
        {
            var item = await _context.States.FirstOrDefaultAsync(s => s.Id == id);

            if (item == null)
            {
                return false;
            }

            _context.States.Remove(item);
            return true;
        }

        public async Task<TaskState> Get(int id)
        {
            return await _context.States.FirstOrDefaultAsync(s => s.Id == id);
        }

        public async Task<IEnumerable<TaskState>> GetAll()
        {
            return await _context.States.ToListAsync();
        }

        public async Task<bool> Update(TaskState item)
        {
            var exists = await _context.States.ContainsAsync(item);

            if (!exists)
            {
                return false;
            }

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
