﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Project.DAL.Repositories
{
    public class TasksRepository : IRepository<Entities.Task>
    {
        private readonly DatabaseContext _context;

        public TasksRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task Create(Entities.Task item)
        {
            await _context.Tasks.AddAsync(item);
        }

        public async Task<bool> Delete (int id)
        {
            var item = await _context.Tasks.FirstOrDefaultAsync(t => t.Id == id);

            if (item == null)
            {
                return false;
            }

            _context.Tasks.Remove(item);
            return true;
        }

        public async Task<Entities.Task> Get(int id)
        {
            return await _context.Tasks.FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<IEnumerable<Entities.Task>> GetAll()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async Task<bool> Update(Entities.Task item)
        {
            var exists = await _context.Tasks.ContainsAsync(item);

            if (!exists)
            {
                return false;
            }

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
