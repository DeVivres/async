﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace Project.DAL.Repositories
{
    public class TeamsRepository : IRepository<Team>
    {
        private readonly DatabaseContext _context;

        public TeamsRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task Create(Team item)
        {
            await _context.Teams.AddAsync(item);
        }

        public async Task<bool> Delete(int id)
        {
            var item = await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);

            if (item == null)
            {
                return false;
            }

            _context.Teams.Remove(item);
            return true;
        }

        public async Task<Team> Get(int id)
        {
            return await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);
        }

        public async Task<IEnumerable<Team>> GetAll()
        {
            return await _context.Teams.ToListAsync();
        }

        public async Task<bool> Update(Team item)
        {
            var exists = await _context.Teams.ContainsAsync(item);

            if (!exists)
            {
                return false;
            }
            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
