﻿using Microsoft.EntityFrameworkCore;
using Project.DAL.Context;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project.DAL.Repositories
{
    public class ProjectsRepository : IRepository<Entities.Project>
    {
        private readonly DatabaseContext _context;

        public ProjectsRepository(DatabaseContext context)
        {
            _context = context; 
        }

        public async Task Create(Entities.Project item)
        {
            await _context.Projects.AddAsync(item);
        }

        public async Task<bool> Delete(int id)
        {
            var item = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);

            if (item == null)
            {
                return false;
            }
            _context.Projects.Remove(item);
            return true;
        }

        public async Task<Entities.Project> Get(int id)
        {
            return await _context.Projects.FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<IEnumerable<Entities.Project>> GetAll()
        {
            return await _context.Projects.ToListAsync();
        }

        public async Task<bool> Update(Entities.Project item)
        {
            var exists = await _context.Projects.ContainsAsync(item);

            if (!exists)
            {
                return false;
            }

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}
