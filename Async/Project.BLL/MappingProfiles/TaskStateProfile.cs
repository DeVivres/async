﻿using AutoMapper;
using Project.Common.DTO;
using Project.DAL.Entities;

namespace Project.BLL.MappingProfiles
{
    public sealed class TaskStateModelProfile : Profile
    {
        public TaskStateModelProfile()
        {
            CreateMap<TaskState, TaskStateDTO>().ReverseMap();
        }
    }
}
