﻿using AutoMapper;
using Project.Common.DTO;
using Project.DAL.Entities;

namespace Project.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>().ReverseMap();
        }
    }
}
