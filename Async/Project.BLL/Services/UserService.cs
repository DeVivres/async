﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Project.BLL.Services
{
    public class UsersService : IService<UserDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UsersService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Create(UserDTO item)
        {
            var mapped = _mapper.Map<User>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            await _unitOfWork.Users.Create(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(int id)
        {
            var result = await _unitOfWork.Users.Delete(id);
            await _unitOfWork.SaveChanges();
            return result;
        }

        public async Task<UserDTO> Get(int id)
        {
            var result = await _unitOfWork.Users.Get(id);
            return _mapper.Map<UserDTO>(result);
        }

        public async Task<IEnumerable<UserDTO>> GetAll()
        {
            var result = await _unitOfWork.Users.GetAll();
            return _mapper.Map<IEnumerable<UserDTO>>(result);
        }

        public async Task<bool> Update(UserDTO item)
        {
            var mapped = _mapper.Map<User>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            await _unitOfWork.Users.Update(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }
    }
}
