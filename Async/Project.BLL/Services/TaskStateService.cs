﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Project.BLL.Services
{
    public class TaskStatesService : IService<TaskStateDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TaskStatesService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Create(TaskStateDTO item)
        {
            var mapped = _mapper.Map<TaskState>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            await _unitOfWork.States.Create(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(int id)
        {
            var result = await _unitOfWork.States.Delete(id);
            await _unitOfWork.SaveChanges();
            return result;
        }

        public async Task<TaskStateDTO> Get(int id)
        {
            var result = await _unitOfWork.States.Get(id);
            return _mapper.Map<TaskStateDTO>(result);
        }

        public async Task<IEnumerable<TaskStateDTO>> GetAll()
        {
            var result = await _unitOfWork.States.GetAll();
            return _mapper.Map<IEnumerable<TaskStateDTO>>(result);
        }

        public async Task<bool> Update(TaskStateDTO item)
        {
            var mapped = _mapper.Map<TaskState>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            await _unitOfWork.States.Update(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }
    }
}
