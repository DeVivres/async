﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Project.BLL.Services
{
    public class ProjectsService : IService<ProjectDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProjectsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Create(ProjectDTO item)
        {
            var mapped = _mapper.Map<DAL.Entities.Project>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            await _unitOfWork.Projects.Create(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(int id)
        {
            var result = await _unitOfWork.Projects.Delete(id);
            await _unitOfWork.SaveChanges();
            return result;
        }

        public async Task<ProjectDTO> Get(int id)
        {
            var result = await _unitOfWork.Projects.Get(id);
            return _mapper.Map<ProjectDTO>(result);
        }

        public async Task<IEnumerable<ProjectDTO>> GetAll()
        {
            var result = await _unitOfWork.Projects.GetAll();
            return _mapper.Map<IEnumerable<ProjectDTO>>(result);
        }

        public async Task<bool> Update(ProjectDTO item)
        {
            var mapped = _mapper.Map<DAL.Entities.Project>(item);
            
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            await _unitOfWork.Projects.Update(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }
    }
}
