﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Project.BLL.Services
{
    public class TasksService : IService<TaskDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TasksService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Create(TaskDTO item)
        {
            var mapped = _mapper.Map<DAL.Entities.Task>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }   

            await _unitOfWork.Tasks.Create(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(int id)
        {
            var result = await _unitOfWork.Tasks.Delete(id);
            await _unitOfWork.SaveChanges();
            return result;
        }

        public async Task<TaskDTO> Get(int id)
        {
            var result = await _unitOfWork.Tasks.Get(id);
            return _mapper.Map<TaskDTO>(result);
        }

        public async Task<IEnumerable<TaskDTO>> GetAll()
        {
            var result = await _unitOfWork.Tasks.GetAll();
            return _mapper.Map<IEnumerable<TaskDTO>>(result);
        }

        public async Task<bool> Update(TaskDTO item)
        {
            var mapped = _mapper.Map<DAL.Entities.Task>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            await _unitOfWork.Tasks.Update(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }
    }
}
