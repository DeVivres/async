﻿using AutoMapper;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using Project.DAL.Entities;
using Project.DAL.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Project.BLL.Services
{
    public class TeamsService : IService<TeamDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TeamsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Create(TeamDTO item)
        {
            var mapped = _mapper.Map<Team>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            await _unitOfWork.Teams.Create(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(int id)
        {
            var result = await _unitOfWork.Teams.Delete(id);
            await _unitOfWork.SaveChanges();
            return result;
        }

        public async Task<TeamDTO> Get(int id)
        {
            var result = await _unitOfWork.Teams.Get(id);
            return _mapper.Map<TeamDTO>(result);
        }

        public async Task<IEnumerable<TeamDTO>> GetAll()
        {
            var result = await _unitOfWork.Teams.GetAll();
            return _mapper.Map<IEnumerable<TeamDTO>>(result);
        }

        public async Task<bool> Update(TeamDTO item)
        {
            var mapped = _mapper.Map<Team>(item);
            var results = new List<ValidationResult>();
            var context = new System.ComponentModel.DataAnnotations.ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true))
            {
                return false;
            }

            await _unitOfWork.Teams.Update(mapped);
            await _unitOfWork.SaveChanges();
            return true;
        }
    }
}
