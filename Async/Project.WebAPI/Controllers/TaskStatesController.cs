﻿using Microsoft.AspNetCore.Mvc;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using System.Threading.Tasks;

namespace Project.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private readonly IService<TaskStateDTO> _taskStatesService;

        public TaskStatesController(IService<TaskStateDTO> statesService)
        {
            _taskStatesService = statesService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _taskStatesService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _taskStatesService.Get(id);

            if (result == null)
            {
                return NotFound(result);
            }
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TaskStateDTO state)
        {
            var result = await _taskStatesService.Create(state);

            if (!result)
            {
                return BadRequest(state);
            }
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] TaskStateDTO state)
        {
            var result = await _taskStatesService.Update(state);

            if (!result)
            {
                return BadRequest(state);
            }
            return Ok(state);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _taskStatesService.Delete(id);

            if (!result)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
