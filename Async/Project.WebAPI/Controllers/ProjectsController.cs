﻿using Microsoft.AspNetCore.Mvc;
using Project.BLL.Interfaces;
using Project.Common.DTO;
using System.Threading.Tasks;

namespace Project.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IService<ProjectDTO> _projecsService;

        public ProjectsController(IService<ProjectDTO> projecsService)
        {
            _projecsService = projecsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()   
        {
            var result = await _projecsService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _projecsService.Get(id);

            if (result == null)
            {
                return NotFound(result);
            }
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ProjectDTO project)
        {
            var result = await _projecsService.Create(project);

            if (!result)
            {
                return BadRequest(project);
            }
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] ProjectDTO project)
        {
            var result = await _projecsService.Update(project);

            if (!result)
            {
                return BadRequest(project);
            }
            return Ok(project);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _projecsService.Delete(id);

            if (!result)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
