﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project.BLL.Interfaces;
using Project.Common.DTO;

namespace Project.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IService<TaskDTO> _tasksService;

        public TasksController(IService<TaskDTO> tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _tasksService.GetAll();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _tasksService.Get(id);

            if (result == null)
            {
                return NotFound(result);
            }
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TaskDTO task)
        {
            var result = await _tasksService.Create(task);

            if (!result)
            {
                return BadRequest(task);
            }
            return Ok();
        }

        [HttpPut]
        public async Task<IActionResult> Update([FromBody] TaskDTO task)
        {
            var result = await _tasksService.Update(task);

            if (!result)
            {
                return BadRequest(task);
            }
            return Ok(task);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _tasksService.Delete(id);

            if (!result)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
